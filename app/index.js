import clock from "clock";
import document from "document";
import { preferences } from "user-settings";
import { HeartRateSensor } from "heart-rate";
import { today } from "user-activity";
import { battery } from "power";
import { vibration } from "haptics";
import * as util from "../common/utils";
import * as messaging from "messaging";


//----------------------------------------------------------------------------------------------------
// UI ELEMENTS                                                                             UI ELEMENTS
//----------------------------------------------------------------------------------------------------

//----------- TIME -----------//
let time_label = document.getElementById("time_label");
//------------ CGM -----------//
let cgm_area = document.getElementById("cgm_area");
let cgm_reading = document.getElementById("cgm_reading");
let cgm_direction = document.getElementById("cgm_direction");
let cgm_duration = document.getElementById("cgm_duration");
//------------ HR ------------//
let heart_rate_area = document.getElementById("heart_rate_area");
let heartRateReading = document.getElementById("heart_rate");
let heartRateDuration = document.getElementById("heart_rate_duration");
//----------- STEPS ----------//
let steps = document.getElementById("steps");

// Set the default hr and steps readings
heartRateReading.text = "--";
heartRateDuration.text = "...";
steps.text = "--";


//----------------------------------------------------------------------------------------------------
// CLOCK                                                                                         CLOCK
//----------------------------------------------------------------------------------------------------

// Update the clock every minute
clock.granularity = "minutes";

// Update the time on every tick with the current time
clock.ontick = (evt) => {
  let today = evt.date;
  let hours = today.getHours();
  if (preferences.clockDisplay === "12h") {
    // 12h format
    hours = hours % 12 || 12;
  } else {
    // 24h format
    hours = util.zeroPad(hours);
  }
  let mins = util.zeroPad(today.getMinutes());
  time_label.text = `${hours}:${mins}`;
}


//----------------------------------------------------------------------------------------------------
// BATTERY                                                                                     BATTERY
//----------------------------------------------------------------------------------------------------

setBattery();

function setBattery() {
  document.getElementById("battery_label").text = (Math.floor(battery.chargeLevel) + "%");
  document.getElementById("battery_level").width = (.3 * Math.floor(battery.chargeLevel));
}

setInterval(setBattery, 60*1000);

//----------------------------------------------------------------------------------------------------
// TIMESTAMPS                                                                               TIMESTAMPS
//----------------------------------------------------------------------------------------------------

// Keep a timestamp of the last readings received. Start when the app is started.
var hr_timestamp = Date.now();
var cgm_timestamp = Date.now();

// This function takes a number of milliseconds and returns a string
// such as "5 min ago".
function convertMsAgoToString(millisecondsAgo) {
  if (millisecondsAgo < 60*1000) {
    return Math.round(millisecondsAgo / 1000) + "s ago";
  }
  else if (millisecondsAgo < 60*60*1000) {
    return Math.round(millisecondsAgo / (60*1000)) + " min ago";
  }
  else {
    var hours = (millisecondsAgo/ (60*60*1000)).toString()[0];
    return hours + "+ h ago";
  }
}

// This function updates the label on the display that shows when data was last updated.
function updateDisplay(display, displayLabelTimestamp) {
  if (displayLabelTimestamp !== undefined) {
    display.text = convertMsAgoToString(Date.now() - displayLabelTimestamp);
  }
}

//----------------------------------------------------------------------------------------------------
// HEART RATE                                                                               HEART RATE
//----------------------------------------------------------------------------------------------------

// Touch event for HR area shows/hides the timestamp
heart_rate_area.onmouseup = function(evt) {
//  console.log("HR TOUCH: " + evt.screenX + ' ' + evt.screenY);
  if (heartRateDuration.style.display == "inline") {
    heartRateDuration.style.display = "none";
  }
  else {
    heartRateDuration.style.display = "inline";
  }
}

// Create a new instance of the HeartRateSensor object
var hrm = new HeartRateSensor();

// Declare a even handler that will be called every time a new HR value is received.
hrm.onreading = function() {
  // Peek the current sensor values
  heartRateReading.text = hrm.heartRate;
  hr_timestamp = Date.now();
}

// Begin monitoring the sensor
hrm.start();

// And update the display every 10 seconds
setInterval(function() { updateDisplay(heartRateDuration, hr_timestamp); }, 10000);


//----------------------------------------------------------------------------------------------------
// STEPS                                                                                         STEPS
//----------------------------------------------------------------------------------------------------

function getSteps() {

  let stepCount = (today.local.steps || 0)+"";

  if(stepCount > 999 && stepCount <= 9999) {
    stepCount = stepCount.substring(0, 1);
    stepCount.trim();
    stepCount += "k"
  } 
  else if(stepCount > 9999) {
    stepCount = stepCount.substring(0, 2);
    stepCount.trim();
    stepCount += "k"
  }

  steps.text = stepCount;
  
}

// Get steps initially
getSteps();

// Get the step count every minute
setInterval(getSteps, 60*1000);

//----------------------------------------------------------------------------------------------------
// CGM                                                                                             CGM
//----------------------------------------------------------------------------------------------------

// Touch event for CGM area
cgm_area.onmouseup = function(evt) {
  
  if (cgm_duration.style.display == "inline") {
    cgm_duration.style.display = "none";
  }
  else {
    cgm_duration.style.display = "inline";
  }
}

// Used to determine if we should alert the user, or if they snoozed
// the alerts
var acceptingAlerts = true;

function alertLow(bloodSugar) {
  
  if (acceptingAlerts) {
    vibration.start("nudge-max");
  
    var low_bg_popup = document.getElementById("low_bg_popup");
    let bgLabel = low_bg_popup.getElementById("BGLabel");
    let bg = bgLabel.getElementById("#copy/text");
    bg.text = "BG: " + bloodSugar;

    // Show the alert popup
    low_bg_popup.style.display = "inline";
    
    // Hide the alert after 30s
    var timeout = setTimeout(function() {low_bg_popup.style.display = "none"; console.log('HIDING ALERT');}, 1*30*1000);

    let btnLeft = low_bg_popup.getElementById("btnLeft");
    let btnRight = low_bg_popup.getElementById("btnRight");

    btnLeft.onclick = function(evt) {
//      console.log("ACKNOWLEDGED LOW BG");
      clearTimeout(timeout);
      low_bg_popup.style.display = "none";
    }
    
    btnRight.onclick = function(evt) {
//      console.log("SNOOZE");
      clearTimeout(timeout);
      snooze();
      low_bg_popup.style.display = "none";
    }
  } 
}

function snooze () {
  // turn alerts off
  acceptingAlerts = false;
  
  // turn them back on in 10 minutes though
  setTimeout(function() {acceptingAlerts = true;}, 10*60*1000);
}

//----------------------------------------------------------------------------------------------------
// MESSAGING                                                                                 MESSAGING
//----------------------------------------------------------------------------------------------------

// Message is received from companion
messaging.peerSocket.onmessage = evt => {
  if (evt.data.cmd == 'CGM') {
    // Update the trend arrow
    cgm_direction.href = "img/arrows/" + evt.data.direction + ".png";
    // Update the CGM reading
    cgm_reading.text = evt.data.BG;
    // Check how old the data is
    cgm_timestamp = evt.data.date;
    // Update the display
    updateDisplay(cgm_duration, cgm_timestamp);
    // See if we need to send an alarm
    if (evt.data.alarm) {
      alertLow(evt.data.BG);
    }
  }
  else if (evt.data.cmd == 'colour') {
    // Get the new fill colour
    let newColour = evt.data.fill;
    
    // Get the new background
    let newBackground = evt.data.background
    
    // Change the background
    let bkg = document.getElementById('background');
    bkg.href = 'img/bkg/' + newBackground;
    
    // Get all the fillable elements and change their colour
    let fills = document.getElementsByClassName("fillable");
    for (var i = 0; i < fills.length; i++) {
      fills[i].style.fill = newColour;
    }
  }
};

// Initialize CGM reading when the app is started
//fetchCGMData();
// Update the timestamp every minute
setInterval(function() {updateDisplay(cgm_duration, cgm_timestamp);}, 1*60*1000);

