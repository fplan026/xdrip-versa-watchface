function mySettings(props) {
  return (
    <Page>
      <Section
        title={<Text bold align="center">API Connections</Text>}>
        <TextInput
          label="Api Endpoint:"
          settingsKey="endpoint"
        />
      </Section>
      
      <Section
        title={<Text bold align="center">Blood Sugar Thresholds</Text>}>
        <Toggle
          settingsKey="lowToggle"
          label="Low BG alarm"
        />
        <TextInput
          label="Low Threshold:"
          settingsKey="lowThreshold"
        />
      </Section>
      <Section
        title={<Text bold align="center">Theme</Text>}>
        <ColorSelect
          settingsKey="appColour"
          colors={[
            {color: '#131c57'},
            {color: '#b9a7c7'},
            {color: '#B6F1C7'},
            {color: 'LightSkyBlue'},
            {color: '#fc7395'},
            {color: 'LightSalmon'}
          ]}
        />
      </Section>
    </Page>
  );
}

registerSettingsPage(mySettings);