import * as messaging from "messaging";
import { settingsStorage } from "settings";
import { me } from "companion"; //needed?

// These are used to let the app know if a blood sugar is too low
var lowToggle = false;
var lowThreshold = 3.0;

// Used to determine when to fetch the next blood sugar
var cgm_timeout;

// Fetch CGM Data from Nightscout Web API
function fetchCGMData()  {
  // Get the URL to fetch from
  let url = getCGMURL();
  
  fetch(url, {
    method: "GET"
  })
  .then(function(res) {
    return res.json();
  })
  .then(function(data) { 
    
    let alarm = false;

    // Determine when we need to fetch the next BG
    var timeSinceLastPull = Date.now() - data[0].date;
    var updateTime = 305000 - timeSinceLastPull;
//    console.log('time since last pull: ' + timeSinceLastPull/60000 + 'minutes');

    // Clear the old timeout just in case it hasn't executed yet
    clearTimeout(cgm_timeout);

    if (updateTime > 0) {
      // Data is less than 5 minutes old
      cgm_timeout = setTimeout(fetchCGMData, updateTime);
//      console.log('scheduling fetch for ' + updateTime + 'ms from now');
    }
    else {
      // Data is older than five minutes, so try again in a minute
      cgm_timeout = setTimeout(fetchCGMData, 60000);
//      console.log('scheduling fetch for 1 minute from now');
    }
    
    // Check to see if the BG is low and if we need to report it
    if (lowToggle) {
      // current BG
      let bg = (data[0].sgv/18.018018).toFixed(1);
      // check if it's lower than the threshold
      if (bg <= lowThreshold) {
//        console.log('LOW: ' + bg + ' < ' + lowThreshold);
        // we need to alert the user
        alarm = true;
      }
      else {
//        console.log('NOT LOW: ' + bg + ' > ' + lowThreshold);
      }
    }
    
    let myData = {
      // The type of message we are sending (a BG reading)
      cmd: 'CGM',
      // Blood sugar in mmo/L
      BG: (data[0].sgv/18.018018).toFixed(1),
      // Arrow direction
      direction: (data[0].direction),
      // Time of last reading
      date: (data[0].date),
      alarm: alarm
    }

    console.log('COMPANION: I fetched :)');

    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
      messaging.peerSocket.send(myData);
    }
  })
  .catch(err => console.log('[FETCH]: ' + err));
}

function changeColour(colour) {
  
  let fill = 'white';
  let bkg = 'pink_geo_background.png';
  
  if (colour == '#131c57') {
    //bleu
    bkg = 'bleu_geo_background.png';
    fill = 'white';
  }
  else if (colour == '#b9a7c7') {
    //lavender
    bkg = 'lavender_geo_background.png';
    fill = '#3e3049';
  }
  else if (colour == '#B6F1C7') {
    //mint
    bkg = 'mint_geo_background.png';
    fill = '#188150';
  }
  else if (colour == 'LightSkyBlue') {
    //paleblue
    bkg = 'paleblue_geo_background.png';
    fill = '#285457';
  }
  else if (colour == 'LightSalmon') {
    //honey
    bkg = 'honey_geo_background.png';
    fill = '#fa1e68';
  }
  else {
    //pink
    bkg = 'pink_geo_background.png';
    fill = '#ff0062';
  }
  
  let myData = {
    cmd: 'colour',
    fill: fill,
    background: bkg
  };
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(myData);
  }
}


// ----------------------------------
// SETTINGS
// ----------------------------------

function getSettings(key) {
  if(settingsStorage.getItem( key )) {
    return JSON.parse(settingsStorage.getItem( key ));
  } else {
    return undefined
  }
}

function getCGMURL() {
  if(getSettings('endpoint').name) {
    return (getSettings('endpoint').name + '/api/v1/entries.json');
  } else {
    // Default xDrip web service 
    console.log('[FETCH]: Using default XDrip Web Service')
    return  "http://127.0.0.1:17580/sgv.json"
  }
}

// A user changes Settings
settingsStorage.onchange = evt => {
  if (evt.key == "endpoint") {
    let data = JSON.parse(evt.newValue);
    fetchCGMData();
  }
  else if (evt.key == "appColour") {
    let colour = JSON.parse(evt.newValue);
    changeColour(colour);
  }
  else if (evt.key == "lowToggle") {
    let data = JSON.parse(evt.newValue);
    lowToggle = data;
    fetchCGMData();
  }
  else if (evt.key == "lowThreshold") {
    let data = JSON.parse(evt.newValue);
    lowThreshold = Number(data.name);
    fetchCGMData();
  }
};

// Restore previously saved settings and send to the device
function restoreSettings() {
  for (let index = 0; index < settingsStorage.length; index++) {
    let key = settingsStorage.key(index);
    if (key && key === "oauth") {
      // We already have an oauth token
      let data = JSON.parse(settingsStorage.getItem(key));
    }
    else if (key && key == "appColour") {
      let data = JSON.parse(settingsStorage.getItem(key));
      changeColour(data);
    }
    else if (key && key == "lowToggle") {
      let data = JSON.parse(settingsStorage.getItem(key));
      lowToggle = data;
    }
    else if (key && key == "lowThreshold") {
      let data = JSON.parse(settingsStorage.getItem(key));
      lowThreshold = Number(data.name);
    }
  }
}

// Message socket opens
messaging.peerSocket.onopen = () => {
  restoreSettings();
  fetchCGMData();
};
